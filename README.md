# Airport Genova

## Prerequisiti
Import dei due database (vedere GRIFOSERVER/LAVORINCORSO)

## Cosa fare dopo clone
- Copia dell'htaccess.sample in .htaccess
- Copia del wp-config-sample.php in wp-config.php
- Copia da produzione della cartella images nella root (se necessario)
- Import dei plugins
- Installazione sistema wp (parte da 5.5.5)

## TODO
analizzare eventuali file .htaccess, perchè sono ignorati e rinominati in .htaccess.sample

## Lista iniziale di plugin
  1.  'advanced-custom-fields/acf.php',  
  2.  'awesome-weather/awesome-weather.php',
  3.  'captcha/captcha.php',
  4.  'classic-editor/classic-editor.php',
  5.  'contact-form-7/wp-contact-form-7.php',
  6.  'cookie-notice/cookie-notice.php',    
  7.  'duplicate-post/duplicate-post.php',
  8.  'facebook-conversion-pixel/facebook-conversion-pixel.php',
  9.  'maintenance-mode-and-under-construction-page',
  10. 'mycheck/mycheck.php',
  11. 'pdf-light-viewer/pdf-light-viewer.php',
  12. 'protection-against-ddos/protection-against-ddos.php',
  13. 'real-media-library/real-media-library.php',
  14. 'remove-http/remove-http.php',
  15. 'sitepress-multilingual-cms/sitepress.php',
  16. 'smtp-mailer/main.php',
  17. 'social-media-feather/social-media-feather.php',
  18. 'tablepress/tablepress.php',
  19. 'wordpress-seo/wp-seo.php',
  20. 'wp-logo-showcase-responsive-slider-slider/logo-showcase.php',
  21. 'wp-migrate-db/wp-migrate-db.php',
  22. 'wpml-string-translation/plugin.php',
  23. 'wpml-translation-management/plugin.php',
