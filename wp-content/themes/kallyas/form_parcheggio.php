	<?php /* Template Name: Parcheggio */ ?>

    
    <link rel="stylesheet" type="text/css" href="/wp-content/plugins/mycheck/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/wp-content/plugins/mycheck/css/jquery-ui.min.css">

    <link rel="stylesheet" type="text/css" href="/wp-content/themes/kallyas-child/style.css">
    <link rel="stylesheet" type="text/css" href="/wp-content/uploads/custom/mycheck/css/custom.css">


    <script type="text/javascript" src="/wp-content/plugins/mycheck/js/jquery.min.js"></script>
    <script type="text/javascript" src="/wp-content/plugins/mycheck/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="/wp-content/plugins/mycheck/js/jquery.chained.js"></script>
    <script type="text/javascript" src="/wp-content/plugins/mycheck/js/jquery.redirect.js"></script>
    <script type="text/javascript" src="/wp-content/plugins/mycheck/js/mycheck.js"></script>

    
    <section id="content" class="site-content parcheggio_iframe" >

            <div class="row">
                <div class="<?php echo $main_class;?>">
                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); the_content();
                        endwhile; else: ?>
                            <p>Sorry, no posts matched your criteria.</p>
                        <?php endif; ?>
                </div><!--// #th-content-page wrapper -->
            </div>

    </section><!--// #content -->
