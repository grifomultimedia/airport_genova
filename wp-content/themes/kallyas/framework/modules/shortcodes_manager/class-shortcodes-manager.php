<?php

class ZnHgTFw_Shortcode_Manager{
	function __construct(){
		// Add shortcode button after media button
		add_action( 'media_buttons', array( $this, 'addMediaButton' ), 999 );
	}

	function enqueueScripts(){
		// Load the css files
		wp_enqueue_style( 'znhgtfw-shortcode-mngr-css', FW_URL .'/assets/css/shortcodes.css', array(), ZN_FW_VERSION );
		// Load the main shortcodes Scripts
		wp_register_script( 'znhgtfw-shortcode-mngr-js', FW_URL .'/assets/js/dist/admin/shortcodes/shortcodes.min.js', array( 'backbone', 'jquery-ui-accordion' ), ZN_FW_VERSION, true );
		// Load extra data
		wp_localize_script( 'znhgtfw-shortcode-mngr-js', 'znhgtfwShortcodesData', $this->getInitialData() ) ;
		// Finally enqueue the script
		wp_enqueue_script( 'znhgtfw-shortcode-mngr-js' );
	}

	/**
	 * Returns a json formatted data
	 * @return json Initial data
	 */
	function getInitialData(){

	}

	/**
	 * Will add the shortcode button after insert media button
	 */
	function addMediaButton(){
		// Only enqueue scripts if the button is added in page
		$this->enqueueScripts();
		echo '<button id="znhgtfw-shortcode-modal-open" class="button">K</button>';
	}
}

new ZnHgTFw_Shortcode_Manager();