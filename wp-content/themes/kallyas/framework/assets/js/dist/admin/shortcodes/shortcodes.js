(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
module.exports = function(obj){
var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};
with(obj||{}){
__p+='<div class="znhgtfw-modal-wrapper">\r\n\t<div class="znhgtfw-modal-header">\r\n\t\t<div class="znhgtfw-modal-title">\r\n\t\t\tThis is the title\r\n\t\t</div>\r\n\t\t<button type="button" class="button-link media-modal-close">\r\n\t\t\t<span class="media-modal-icon">\r\n\t\t\t\t<span class="screen-reader-text">Close media panel</span>\r\n\t\t\t</span>\r\n\t\t</button>\r\n\t</div>\r\n\t<div class="znhgtfw-modal-content-wrapper">\r\n\t\t<div class="znhgtfw-modal-sidebar"></div>\r\n\t\t<div class="znhgtfw-modal-content">\r\n\t\t\t<div class="znhgtfw-shortcode-mngr-nothing-selected">\r\n\t\t\t\t<p>Choose a shortcode from the sidebar to get started.</p>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n\t<div class="znhgtfw-modal-footer">\r\n\t\t<div class="znhgtfw-footer-nav">\r\n\t\t\t<a href="#" class="znhgtfw-button">Save changes</a>\r\n\t\t\t<a href="#" class="znhgtfw-button znhgtfw-button--danger">Close</a>\r\n\t\t</div>\r\n\t</div>\r\n</div>\r\n<div class="znhgtfw-modal-backdrop"></div>';
}
return __p;
};

},{}],2:[function(require,module,exports){
module.exports = Backbone.Model.extend({
	defaults : {
		id : 'shortcode-tag',
		name : 'Shortcode Name',
		section : 'Section',
		description : 'Shortcode description',
		params : [],
	},
	setSelected:function() {
		this.collection.setSelected(this);
	}
});
},{}],3:[function(require,module,exports){
var ShortcodesCollection = Backbone.Collection.extend({
	model: require('./shortcodeModel'),
	initialize: function() {
		this.selected = null;
	},
	bySection : function(sectionName){
		filtered = this.filter(function ( shortcode ) {
			return shortcode.get('section') === sectionName;
		});
		return new ShortcodesCollection(filtered);
	},
	setSelected: function( shortcode ) {
		if (this.selected) {
			this.selected.set({selected:false});
		}
		shortcode.set({selected:true});
		this.selected = shortcode;
		this.trigger('shortcodeSelected');
	},
	getSelected : function(){
		return this.selected;
	}
});

module.exports = ShortcodesCollection;
},{"./shortcodeModel":2}],4:[function(require,module,exports){
window.znhg = window.znhg || {};

znhgShortcodesManagerData = {};
znhgShortcodesManagerData.sections = [
	'Typography',
	'Tables',
];

znhgShortcodesManagerData.shortcodes = [
	{
		id : 'heading',
		name : 'Heading',
		section : 'Typography',
		description : 'This is the shortcode description',
		params : [
			{
				name : 'This is the option name',
				description : 'This is the option description',
				type : 'text',
			}
		],
	},
	{
		id : 'heading2',
		name : 'Heading2',
		section : 'Typography',
		description : 'This is the shortcode description',
		params : [
			{
				name : 'This is the option name',
				description : 'This is the option description',
				type : 'text',
			}
		],
	}
];

(function ($) {
	var App = function(){},
		ModalView = require('./views/modal'),
		ShortcodesCollection = require('./models/shortcodesCollection');

	/**
	 * Starts the main shortcode manager class
	 */
	App.prototype.start = function(){
		// Bind the click event
		$(document).on('click', '#znhgtfw-shortcode-modal-open', function(e){
			e.preventDefault();
			this.openModal();
		}.bind(this));

		this.shortcodesCollection = new ShortcodesCollection(znhgShortcodesManagerData.shortcodes);

		// Allow chaining
		return this;
	};

	/**
	 * Opens the modal window
	 */
	App.prototype.openModal = function(){
		// Only allow an instance of the modalView
		if( this.modalView === undefined ){
			this.modalView = new ModalView({collection: this.shortcodesCollection, app : this});
		}
	};

	/**
	 * Opens the modal window
	 */
	App.prototype.closeModal = function(){
		this.modalView = undefined;
	};

	znhg.shortcodesManager = new App().start();

})(jQuery);
},{"./models/shortcodesCollection":3,"./views/modal":5}],5:[function(require,module,exports){
var navView = require('./navView');

module.exports = Backbone.View.extend({
	id: "znhgtfw-shortcodes-modal",
	template : require('../html/modal.html'),
	events : {
		'click .znhgtfw-modal-backdrop' : 'modalClose',
		'click .media-modal-close' : 'modalClose'
	},
	initialize : function( options ){
		this.mainApp = options.app;
		console.log(this.collection);
		this.listenTo(this.collection, 'shortcodeSelected', this.renderParams);
		this.render();
	},
	render : function(){
		this.$el.html( this.template() );

		// Add the navigation
		this.$('.znhgtfw-modal-sidebar').append( new navView().render().$el );

		// Finally.. add the modal to the page
		jQuery( 'body' ).append( this.$el ).addClass('znhgtfw-modal-open');

		return this;
	},
	modalClose : function(){
		this.$el.remove();
		jQuery('body').removeClass('znhgtfw-modal-open');
		this.mainApp.closeModal();
	},
	renderParams: function(){
		alert('changed');
	}
});
},{"../html/modal.html":1,"./navView":8}],6:[function(require,module,exports){
module.exports = Backbone.View.extend({
	tagName : 'li',
	events : {
		'click' : 'selectShortcode'
	},
	render : function(){
		this.$el.html( jQuery('<a href="#">' + this.model.get('name') + '</a>') );
		return this;
	},
	selectShortcode : function(){
		alert('clicked');
		this.model.setSelected();
	}
});
},{}],7:[function(require,module,exports){
var navItem = require('./navItem');
module.exports = Backbone.View.extend({
	tagName: 'ul',
	className : 'znhgtfw-modal-menu-dropdown',
	render : function(){
		this.collection.each(function( shortcode ){
			this.$el.append(new navItem({model: shortcode}).render().$el);
		}.bind(this));
		return this;
	}
});
},{"./navItem":6}],8:[function(require,module,exports){
var navSection = require('./navSection');
module.exports = Backbone.View.extend({
	tagName: 'ul',
	className : 'znhgtfw-modal-menu',
	events : {
		'click > li > a' : 'toggleSection'
	},
	render : function(){
		_(znhgShortcodesManagerData.sections).each(function(sectionName){
			var $li = jQuery('<li></li>');
			$li.append('<a href="#">'+ sectionName +'</a>');
			$li.append( new navSection( { collection: znhg.shortcodesManager.shortcodesCollection.bySection( sectionName ) } ).render().$el );
			this.$el.append($li);
		}.bind(this));
		return this;
	},
	toggleSection : function(e){
		this.$el.find('li').removeClass('active');
		jQuery(e.target).parent().addClass('active');
	}
});
},{"./navSection":7}]},{},[4]);
