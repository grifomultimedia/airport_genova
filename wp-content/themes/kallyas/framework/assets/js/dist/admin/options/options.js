(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
window.znhg = window.znhg || {};

(function ($){
	var App = function(){};

	App.prototype.start = function(){
		// Register default option types
		this.registerDefaultOptions();
	};

	App.prototype.registerDefaultOptions = function(){
		// Will register a new option type
		defaultOptions = {
			'text' : require('./views/options/text')
		};

		_(defaultOptions).each(function(optionId, optionView){
			this.registerOption( optionId, optionView );
		});
	};

	App.prototype.registerOption = function( optionsType, args ){
		// Will register a new option type
	};

	App.prototype.unregisterOption = function(){
		// Will unregister a new option type
	};

	App.prototype.renderForm = function(){
		// Will rener a form that has saving capabilities
	};

	App.prototype.renderOptionsGroup = function( args ){
		// Will render a group of options
	};

	znhg.optionsMachine = new App().start();
}(jQuery));
},{"./views/options/text":2}],2:[function(require,module,exports){

},{}]},{},[1]);
