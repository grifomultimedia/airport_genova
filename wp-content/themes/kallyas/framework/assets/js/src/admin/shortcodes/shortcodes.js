window.znhg = window.znhg || {};

znhgShortcodesManagerData = {};
znhgShortcodesManagerData.sections = [
	'Typography',
	'Tables',
];

znhgShortcodesManagerData.shortcodes = [
	{
		id : 'heading',
		name : 'Heading',
		section : 'Typography',
		description : 'This is the shortcode description',
		params : [
			{
				name : 'This is the option name',
				description : 'This is the option description',
				type : 'text',
			}
		],
	},
	{
		id : 'heading2',
		name : 'Heading2',
		section : 'Typography',
		description : 'This is the shortcode description',
		params : [
			{
				name : 'This is the option name',
				description : 'This is the option description',
				type : 'text',
			}
		],
	}
];

(function ($) {
	var App = function(){},
		ModalView = require('./views/modal'),
		ShortcodesCollection = require('./models/shortcodesCollection');

	/**
	 * Starts the main shortcode manager class
	 */
	App.prototype.start = function(){
		// Bind the click event
		$(document).on('click', '#znhgtfw-shortcode-modal-open', function(e){
			e.preventDefault();
			this.openModal();
		}.bind(this));

		this.shortcodesCollection = new ShortcodesCollection(znhgShortcodesManagerData.shortcodes);

		// Allow chaining
		return this;
	};

	/**
	 * Opens the modal window
	 */
	App.prototype.openModal = function(){
		// Only allow an instance of the modalView
		if( this.modalView === undefined ){
			this.modalView = new ModalView({collection: this.shortcodesCollection, app : this});
		}
	};

	/**
	 * Opens the modal window
	 */
	App.prototype.closeModal = function(){
		this.modalView = undefined;
	};

	znhg.shortcodesManager = new App().start();

})(jQuery);