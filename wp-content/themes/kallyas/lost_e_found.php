	<?php /* Template Name: Lost&Found */
//echo ICL_LANGUAGE_CODE; 
$titolo;
$sottotitolo;
$code_descr;
$btn;
?>

    
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/kallyas/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/kallyas/css/template.css">
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/kallyas-child/style.css">
    <section id="content" class="site-content lost_found_iframe" >
        <div class="container">
            <div class="row">
                <div class="<?php echo $main_class;?>">
                    <div id="th-content-page">
                        <?php 
                          switch(ICL_LANGUAGE_CODE)
                          {
                            case 'en':
                              $titolo ="BAGGAGE STATUS UPDATE";
                              $sottotitolo ="For information on your luggage, please enter the code you were given by airport staff.";
                              $code_descr="WEB AND CALL CENTER CODE";
                              $btn="Search";
                              break;
                            default:
                              $titolo ="AGGIORNAMENTO STATUS PRATICA";
                              $sottotitolo ="Per informazioni sul tuo bagaglio, inserisci il codice che ti è stato fornito dal personale dell'aeroporto.";
                              $code_descr="NUMERO INTERNET E CALL CENTER";
                              $btn="Cerca";
                          };



                            echo "<div id='lost_found_form'><h1>".$titolo."</h1>";
                            echo "<p>".$sottotitolo."</p>";
                            echo "<form action='/wp-content/uploads/custom/lostAndFound/lostAndFound.php' method='post'>
                              <span class='codice_wt'>".$code_descr."</span><br><input type='text' placeholder='es. 12345' class='nl-email kl-newsletter-field form-control' name='searchCode' value=''>";
                            echo "<input type='hidden' name='language' value='".ICL_LANGUAGE_CODE."'>";
                            echo "<input type='submit' class='nl-submit kl-newsletter-submit kl-font-alt btn btn-fullcolor' value='".$btn."'></form></div>";



                         ?>




                         <!-- 
                         <div id="lost_found_form">
                            <h1>AGGIORNAMENTO STATUS PRATICA</h1>
                            <p>Per informazioni sul tuo bagaglio, inserisci il codice che ti è stato fornito dal personale dell'aeroporto</p>
                            
                            <form action="/wp-content/uploads/custom/lostAndFound/lostAndFound.php" method="post">
                              <span class="codice_wt">Codice World Tracer</span><br><input type="text" placeholder="es. GOAAZ12328" class="nl-email kl-newsletter-field form-control" name="searchCode" value="">
                              <?php 
                              //echo "<input type='hidden' name='language' value='".ICL_LANGUAGE_CODE."'>";
                              ?>
                              <input type="submit" class="nl-submit kl-newsletter-submit kl-font-alt btn btn-fullcolor" value="Cerca">
                              </form>
                        </div>
                        -->



                        
                    </div><!--// #th-content-page -->
                </div><!--// #th-content-page wrapper -->
            </div>
        </div>
    </section><!--// #content -->
