// Project configuration
var project  	 = 'kallyas', // Project name, used for build zip.
	url          = 'localhost/kallyas_demo', // Local Development URL for BrowserSync. Change as-needed.
	build        = './buildtheme/', // Files that you want to package into a zip go here
	// Path to all PHP files.
	projectPHPWatchFiles = [
		'./**/*.php',
		'!buildtheme/**/*',
	],
	buildInclude = [
		// include common file types
		'**/*.*',

		// exclude files and folders
		'!node_modules/**/*',
		'!assets/bower_components/**/*',
		'!style.css.map',
		'!assets/js/custom/*',
		'!assets/css/patrials/*',
		'!package.json',
		'!build.bat',
		'!README.md',
		'!buildtheme/**/*',
		'!' + project + '.zip',
	];

// Translation related.
var text_domain   = 'zn_framework';
var destFile      = 'en_US.pot';
var team          = 'Hogash';
var translatePath = './languages';


// Node modules
var gulp 		= require('gulp'),
	zip         = require('gulp-zip'),
	runSequence = require('run-sequence'),
	sort        = require('gulp-sort'),
	wpPot       = require('gulp-wp-pot'),
	bump        = require('gulp-bump'),
	prompt      = require('gulp-prompt'),
	replace     = require('gulp-replace'),
	del     	= require('del'),

	// JS
	browserify  = require('browserify'),
	concat      = require('gulp-concat'),
	uglify      = require('gulp-uglify'),
	rename      = require('gulp-rename'),
	path         = require('path'),
	source = require('vinyl-source-stream'),
	underscorify = require('node-underscorify'),
	gutil = require('gulp-util'),
	buffer = require('vinyl-buffer'),

	notify      = require('gulp-notify');

/**
 * Build task that moves essential theme files for production-ready sites
 *
 * buildFiles copies all the files in buildInclude to build folder - check variable values at the top
 * buildImages copies all the images from img folder in assets while ignoring images inside raw folder if any
 */

gulp.task('buildFiles', function() {
	return  gulp.src(buildInclude)
				.pipe(gulp.dest(build))
				.pipe(notify({ message: 'Copy from buildFiles complete', onLast: true }));
});

 /**
 * Zipping build directory for distribution
 *
 * Taking the build folder, which has been cleaned, containing optimized files and zipping it up to send out as an installable theme
*/
gulp.task('buildZip', function () {
   return 	gulp.src(build+'/**/')
			   .pipe(zip(project+'.zip'))
			   .pipe(gulp.dest('./'))
			   .pipe(notify({ message: 'Zip task complete', onLast: true }));
});

/**
 * WP POT Translation File Generator.
 *
 * * This task does the following:
 *     1. Gets the source of all the PHP files
 *     2. Sort files in stream by path or any custom sort comparator
 *     3. Applies wpPot with the variable set at the top of this file
 *     4. Generate a .pot file of i18n that can be used for l10n to build .mo file
 */
gulp.task( 'translate', function () {
	return gulp.src( projectPHPWatchFiles )
		.pipe(sort())
		.pipe(wpPot( {
			domain        : text_domain,
			destFile      : destFile,
			package       : project,
			team          : team
		} ))
	   .pipe(gulp.dest(translatePath))
	   .pipe( notify( { message: 'TASK: "translate" Completed! 💯', onLast: true } ) );

});

gulp.task('changeVersion', bumpDialogTask);
function bumpDialogTask(callback){
	var target = './*'; // project root
	gulp.src(target).pipe(prompt.prompt({
		  type: 'input',
		  name: 'version',
		  message: 'What type of version number you are releasing?',
		},
		function(response){
			gulp.src(['style.css'])
				.pipe(replace(/(Version: )(\d+.\d+.\d+(.\d+)?)/g, '$1'+response.version))
				.pipe(gulp.dest('./'))
				.pipe(notify({ message: 'Version change complete', onLast: true })
			);
			callback();
		})
	);
}

gulp.task('cleanup', function(){
	return del(build);
});

/**
 * Scripts: Vendors
 *
 * Look at src/js and concatenate those files, send them to assets/js where we then minimize the concatenated file.
*/
gulp.task('vendorsJs', function() {

	var vendorsPaths = [
		// Admin shortcodes vendor
		//'./framework/assets/js/src/admin/shortcodes/vendors/'
	];

	var tasks = vendorsPaths.map(function(folder) {

		// Replace src with dist
		var distPath = folder.replace('src','dist'),
		// Get the final filename name
			dirname  = path.basename(distPath);

		return gulp.src(folder + '*.js')
			// concat into foldername.js
			.pipe(concat(dirname + '.js'))
			// write to output
			.pipe(gulp.dest(distPath))
			// minify
			.pipe(uglify())
			// rename to folder.min.js
			.pipe(rename(dirname + '.min.js'))
			// write to output again
			.pipe(gulp.dest(distPath));
	});

});

/**
 * Scripts: Browserify
 *
 * Look at src/MODULE_NAME/MODULE_NAME.js and browserify the file, send them to build folder.
*/
gulp.task('browserify', function() {

	var appPaths = [
		// Admin shortcodes app
		'./framework/assets/js/src/admin/shortcodes/',
		'./framework/assets/js/src/admin/options/'
	];

	var tasks = appPaths.map(function(folder) {

		// Replace src with dist
		var distPath = folder.replace('src','dist'),
			// Get the final filename name
			dirname  = path.basename(distPath);

			return browserify(folder + dirname +'.js')
				.on('error', gutil.log)
				.transform(underscorify)
				.bundle()
				// Pass desired output filename to vinyl-source-stream
				.pipe(source(dirname+'.js'))
				// Start piping stream to tasks!
				.pipe(gulp.dest(distPath))
				.pipe(buffer())
				// Minimify the code
				.pipe(uglify())
				// rename to folder.min.js
				.pipe(rename(dirname+'.min.js'))
				// write to output again
				.pipe(gulp.dest(distPath));

	});

});

// Watch Task
gulp.task('default', function (){
	gulp.watch('./framework/assets/js/src/**/*.js', ['browserify', 'vendorsJs']);
	gulp.watch('./framework/assets/js/src/**/*.html', ['browserify']);
});





















// ==== TASKS ==== //
/**
 * Gulp Default Task
 *
 * Compiles styles, fires-up browser sync, watches js and php files. Note browser sync task watches php files
 *
*/

// Package Distributable Theme
gulp.task('build', function(cb) {
	runSequence('translate',  'changeVersion', 'buildFiles', 'buildZip', 'cleanup');
});