<?php if(! defined('ABSPATH')){ return; }
/*
Name: Price List
Description: This element will generate a list containing prices
Class: ZnPriceList
Category: content
Keywords: restaurant, menu
Level: 3
*/


class ZnPriceList extends ZnElements {

	public static function getName(){
		return __( "Price List", 'zn_framework' );
	}
	function options() {

		$uid = $this->data['uid'];


		// TODO
		// single_item: is featured
		// single_item: image
		// image display style - on left with click to open // on hover as a tooltip
		// extra style with more fields (for a pizza joint eg: large / medium / small)

		// OTHERS
		// custom button

		$options = array(
			'css_selector' => '.',
			'has_tabs'  => true,
			'general' => array(
				'title' => 'General options',
				'options' => array(

					array(
						"id"          => "pl_curr",
						"name"        => "Currency",
						"description" => "Please enter the currency symbol or text.",
						"std"         => "",
						"type"        => "text",
						"placeholder" => "eg: $",
					),

					array(
						"id"          => "pl_curr_pos",
						"name"        => "Currency Position",
						"description" => "Please enter the currency symbol or text.",
						"std"         => "before",
						"type"        => "select",
						'options'     => array(
							'before' => 'Before',
							'after' => 'After'
						),
					),

					array (
						"name"        => __( "Typography settings", 'zn_framework' ),
						"description" => __( "Specify the typography properties for the title and price.", 'zn_framework' ),
						"id"          => "title_typo",
						"std"         => '',
						'supports'   => array( 'size', 'font', 'style', 'line', 'color', 'weight' ),
						"type"        => "font",
					),

					array (
						"name"        => __( "Description Typography settings", 'zn_framework' ),
						"description" => __( "Specify the typography properties for the description.", 'zn_framework' ),
						"id"          => "desc_typo",
						"std"         => '',
						'supports'   => array( 'size', 'font', 'style', 'line', 'color', 'weight' ),
						"type"        => "font",
					),

					array(
						"id"          => "pl_price_color",
						"name"        => "Prices Color",
						"description" => "Please choose the price default color.",
						"std"         => "#cd2122",
						"alpha"       => "true",
						"type"        => "colorpicker",
					),

					array(
						'id'          => 'vertical_spacing',
						'name'        => 'Vertical Spacing',
						'description' => 'Select the vertical spacing ( in pixels ) for the item list.',
						'type'        => 'slider',
						'std'         => '5',
						'class'       => 'zn_full',
						'helpers'     => array(
							'min' => '0',
							'max' => '50',
							'step' => '1'
						),
						'live' => array(
							'multiple' => array(
								array(
									'type'      => 'css',
									'css_class' => '.'.$uid. ' > ul > li',
									'css_rule'  => 'margin-top',
									'unit'      => 'px'
								),
								array(
									'type'      => 'css',
									'css_class' => '.'.$uid. ' > ul > li',
									'css_rule'  => 'margin-bottom',
									'unit'      => 'px'
								),
								array(
									'type'      => 'css',
									'css_class' => '.'.$uid. '.priceListElement-dash--separator > ul > li',
									'css_rule'  => 'padding-bottom',
									'unit'      => 'px'
								),
							)
						)
					),

					array(
						'id'          => 'dotted_line',
						'name'        => 'Dotted line style',
						'description' => 'Select the style of the dotted line.',
						'type'        => 'select',
						'std'         => 'classic',
						'options'        => array(
							'classic' => 'Classic',
							'separator' => 'As item separator',
						)
					),

					array(
						'id'          => 'element_scheme',
						'name'        => 'Element Color Scheme',
						'description' => 'Select the color scheme of this element',
						'type'        => 'select',
						'std'         => '',
						'options'        => array(
							'' => 'Inherit from Kallyas options > Color Options [Requires refresh]',
							'light' => 'Light (default)',
							'dark' => 'Dark'
						),
						'live'        => array(
							'multiple' => array(
								array(
									'type'      => 'class',
									'css_class' => '.'.$uid,
									'val_prepend'  => 'priceListElement-scheme--',
								),
								array(
									'type'      => 'class',
									'css_class' => '.'.$uid,
									'val_prepend'  => 'element-scheme--',
								),
							)
						)
					),

				)
			),
			'price_items' => array(
				'title' => 'Items',
				'options' => array(

					array(
						'id'            => 'price_list',
						'name'          => 'Price list items',
						'description'   => 'Here you can add price list items',
						'type'          => 'group',
						'sortable'      => true,
						'element_title' => 'Item',
						'subelements'   => array(
							array(
								"id"          => "pl_title",
								"name"        => "Title",
								"description" => "Please enter the title that will appear on the left side.",
								"std"         => "",
								"type"        => "text",
								"class"        => "zn_input_xl",
							),
							array(
								"id"          => "pl_price",
								"name"        => "Price",
								"description" => "Please enter the price that will appear on the right side.",
								"std"         => "",
								"type"        => "text"
							),
							array(
								"id"          => "pl_desc",
								"name"        => "Description",
								"description" => "Please enter the description that will appear under the price.",
								"std"         => "",
								"type"        => "textarea"
							),

							// array(
							// 	"id"          => "pl_desc",
							// 	"name"        => "Is featured?",
							// 	"description" => "Please enter the description that will appear under the price.",
							// 	"std"         => "",
							// 	"type"        => "textarea"
							// ),

							array(
								"id"          => "pl_title_color",
								"name"        => "Title Color",
								"description" => "Select if you want to override the default title color.",
								"std"         => "",
								"alpha"       => "true",
								"type"        => "colorpicker",
							),

							array(
								"id"          => "pl_price_color",
								"name"        => "Price Color",
								"description" => "Select if you want to override the default price color.",
								"std"         => "",
								"alpha"       => "true",
								"type"        => "colorpicker",
							),

						),
					),

				),
			),

		);

		return $options;
	}

	function element() {

		$options = $this->data['options'];


		//Class
		$classes = array();
		$classes[] = $uid = $this->data['uid'];

		$classes[] = zn_get_element_classes($options);

		$color_scheme = $this->opt( 'element_scheme', '' ) == '' ? zget_option( 'zn_main_style', 'color_options', false, 'light' ) : $this->opt( 'element_scheme', '' );
		$classes[] = 'priceListElement-scheme--'.$color_scheme;
		$classes[] = 'element-scheme--'.$color_scheme;

		$classes[] = 'priceListElement-dash--'.$this->opt( 'dotted_line', 'classic' );

		$attributes = zn_get_element_attributes($options);

		?>

		<div class="priceListElement <?php echo implode(' ', $classes); ?> " <?php echo $attributes; ?>>
			<ul>
				<?php

				$currency = $this->opt('pl_curr', '');
				$currency_position = $this->opt('pl_curr_pos', 'before');

				$priceItems    = $this->opt( 'price_list' );

				// Set some defaults for buttons
				if( empty( $priceItems ) ){
					$priceItems = array(
						array(
							'pl_title' => 'Some title right here',
							'pl_price' => '79.99',
							'pl_desc' => 'Just some description, can be empty if you want.',
						),
					);
				}
				if(is_array($priceItems) && !empty($priceItems)){
					foreach ( $priceItems as $i => $entry ) {

						if($currency_position == 'before'){
							$pbefore = $currency;
							$pafter = '';
						}
						elseif($currency_position == 'after'){
							$pbefore = '';
							$pafter = $currency;
						}

						$title    = ! empty( $entry['pl_title'] ) ? '<h4 class="priceListElement-itemTitle">'.$entry['pl_title'].'</h4>' : '';
						$price    = ! empty( $entry['pl_price'] ) ? '<div class="priceListElement-itemPrice">'.$pbefore.$entry['pl_price'].$pafter.'</div>' : '';
						$desc    = ! empty( $entry['pl_desc'] ) ? '<div class="priceListElement-itemDesc">'.$entry['pl_desc'].'</div>' : '';

						?>
						<li class="priceListElement-item-<?php echo $i; ?>">

							<div class="priceListElement-itemMain">
								<?php echo $title; ?>
								<div class="priceListElement-dottedSeparator"></div>
								<?php echo $price; ?>
								<?php
									if($this->opt( 'dotted_line', 'classic' ) == 'separator') echo '<div class="clearfix"></div>';
								?>
							</div>

							<?php echo $desc; ?>

                  			<div class="clearfix"></div>
						</li>
					<?php
					} // end foreach
				}
				?>
			</ul>
			<div class="clearfix"></div>
		</div>
	<?php
	}

	function css(){

		$uid = $this->data['uid'];
		$css = '';

		$vertical_spacing = $this->opt('vertical_spacing', 5);
		$ttl_bmargin = $vertical_spacing != '5' ?  : '';
		if( $vertical_spacing != '' && $vertical_spacing != 5 ){
			$css .= '.'.$uid.' > ul > li{margin-top:'.$vertical_spacing.'px; margin-bottom:'.$vertical_spacing.'px;}';
			$css .= '.'.$uid.'.priceListElement-dash--separator > ul > li{padding-bottom:'.$vertical_spacing.'px;}';
		}

		// Title Styles
		$title_styles = '';
		$title_typo = $this->opt('title_typo');
		if( is_array($title_typo) && !empty($title_typo) ){
			foreach ($title_typo as $key => $value) {
				if($value != '') {
					if( $key == 'font-family' ){
						$title_styles .= $key .':'. zn_convert_font($value).';';
					}
					else {
						$title_styles .= $key .':'. $value.';';
					}
				}
			}
			if(!empty($title_styles)){
				$css .= '.'.$uid.' .priceListElement-itemTitle, .'.$uid.' .priceListElement-itemPrice {'.$title_styles.'}';
			}
			// Make proper dotted separator
			$font_size = !empty($title_typo['font-size']) ? $title_typo['font-size'] : 14;
			$line_height = !empty($title_typo['line-height']) ? $title_typo['line-height'] : 24;
			$css .= '.'.$uid.' .priceListElement-dottedSeparator {margin-bottom: calc(('.(int)$line_height.'px - '.(int)$font_size.'px) / 2);}';
		}

		$pl_price_color = $this->opt('pl_price_color', '#cd2122');
		if($pl_price_color != '#cd2122'){
			$css .= '.'.$uid.' .priceListElement-itemPrice {color:'.$pl_price_color.'}';
		}

		// Subtitle styles
		$desc_styles = '';
		$desc_typo = $this->opt('desc_typo');
		if( is_array($desc_typo) && !empty($desc_typo) ){
			foreach ($desc_typo as $key => $value) {
				if($value != '') {
					if( $key == 'font-family' ){
						$desc_styles .= $key .':'. zn_convert_font($value).';';
					} else {
						$desc_styles .= $key .':'. $value.';';
					}
				}
			}
			if(!empty($desc_styles)){
				$css .= '.'.$uid.' .priceListElement-itemDesc{'.$desc_styles.'}';
			}
		}

		// color per items
		$priceItems    = $this->opt( 'price_list' );
		if(is_array($priceItems) && !empty($priceItems)){
			foreach ( $priceItems as $i => $entry ) {
				if(isset($entry['pl_title_color']) && !empty($entry['pl_title_color'])){
					$css .= '.'.$uid.' .priceListElement-item-'.$i.' .priceListElement-itemTitle {color:'.$entry['pl_title_color'].'}';
				}
				if(isset($entry['pl_price_color']) && !empty($entry['pl_price_color'])){
					$css .= '.'.$uid.' .priceListElement-item-'.$i.' .priceListElement-itemPrice {color:'.$entry['pl_price_color'].'}';
				}
			}
		}



		return $css;


	}

}
