<?php
include("wp-content/uploads/custom/configura_db.php");


$oggi = date('d-m-Y');
$adesso = date('H:i');

// ICL_LANGUAGE_CODE variabile predefinita di WPML, restituisce il codice della lingua (es. "it", "en"...)
switch (ICL_LANGUAGE_CODE) {
	case 'en':
		$Volo = 'CODE';
		$Provenienza = 'DESTINATION';
		$Orario = 'SCHEDULED';
		$Previsto = 'EXPECTED';
		$Note = 'STATUS';
		$Compagnia = 'AIRLINES';
		$titolo_part = "DEPARTURES";
		$titolo_arr = "ARRIVALS";
		$link = 'ALL DEPARTURES';
		break;
	default:
		$Volo = 'VOLO';
		$Provenienza = 'DESTINAZIONE';
		$Orario = 'PREVISTO';
		$Previsto = 'ORARIO';
		$Note = 'STATUS';
		$Compagnia = 'COMPAGNIA';
		$titolo_part = "PARTENZE";
		$titolo_arr = "ARRIVI";
		$link = 'TUTTE LE PARTENZE';
}

echo "
<div id='posizione'>
	<table width='100%' cellspacing=0 cellpadding=0 border='0' id='orario_arrivi' class='orariotemporeale'>
		<tr class='testo' style='background-color: #cd2122; color: #FFF; height: 40px;'>
			<th width='20%' style='text-align:left;'>$Provenienza</th>
			<th width='12%'>$Previsto</th>
			<th width='12%'>$Orario</th>
			<th width='16%'>$Volo</th>
			<th width='20%'>$Compagnia</th>
			<th width='20%'>$Note</th>
		</tr></table>
		";

echo "<div id='inner-table-voli'><table width='100%' cellspacing=0 cellpadding=0 border='0' id='orario_arrivi_center' class='orariotemporeale'>";

$html_code = "";

$i = 20;
$oggi = date('Y-m-d');
$domani = date('Y-m-d', strtotime($oggi . ' +1 day'));
$adesso = date('Hi');

$sql0 = "SELECT DATE_FORMAT(curtime(), '%H%i')";
$rs0 = mysqli_query($conn, $sql0);
$r = mysqli_fetch_row($rs0);
$adesso = $r[0];


//$sql = "SELECT * FROM partenze_new WHERE previsto>'".$adesso."' and data IN ('".$oggi."','".$domani."') GROUP BY previsto, routing_uno,gate ORDER BY data, schedulato";
$sql = "SELECT * FROM partenze_new WHERE previsto>'" . $adesso . "' and data = '" . $oggi . "' GROUP BY previsto, routing_uno,gate UNION SELECT * FROM partenze_new WHERE data = '" . $domani . "' GROUP BY previsto, routing_uno,gate";
//echo $sql;
$rs = mysqli_query($conn, $sql);

if ($rs) {
	while ($row_xy = mysqli_fetch_row($rs)) {

		//$destinazione = $row_xy[8];
		$destinazione = (ICL_LANGUAGE_CODE == 'en') ? $row_xy[9] : $row_xy[8];
		$destinazione_visibile = $destinazione;
		//$arr_destinazioni = explode(' ', $destinazione, 2);
		//$stato_volo = $row_xy[12];
		$stato_volo = (ICL_LANGUAGE_CODE == 'en') ? $row_xy[17] : $row_xy[12];
		$bgcolor = ($i % 2 == 0) ? '#f5f5f5' : '#ffffff';
		$previsto = substr($row_xy[10], 0, 2) . ':' . substr($row_xy[10], -2);
		$effettivo = substr($row_xy[11], 0, 2) . ':' . substr($row_xy[11], -2);
		$abbrevizione_aeroporto = $row_xy[25];
		$gate = $row_xy[20];

		$array_nomi_aeroporti = array('FIUMICINO', 'STANSTED', 'GATWICK', 'BARAJAS', 'C. DE GAULLE');
		foreach ($array_nomi_aeroporti as $nome_aeroporto) {
			if (strpos($destinazione, $nome_aeroporto) > 0) $destinazione_visibile = str_replace($nome_aeroporto, '', $destinazione);
		}

		$arr_codici_volo = array();
		$arr_bandiera = array();
		$html_code_codici_volo = '';
		$html_code_bandiere = '';

		$sql = "SELECT volo_uno, vettore_uno FROM partenze_new ";
		$sql .= "WHERE (routing_uno='" . $destinazione . "' OR routing_due='" . $destinazione . "') AND schedulato='" . $row_xy[10] . "' AND gate='" . $gate . "'";
		//echo $sql.'<br/>';
		$rs2 = mysqli_query($conn, $sql);
		while ($arr = mysqli_fetch_array($rs2)) {

			$bandiera = '';
			$vettore = $arr['vettore_uno'];
			$codice_volo = $arr['volo_uno'];

			#echo ' - '.$codice_volo.' - '.$vettore.'<br/>';

			if ($vettore != '')
				$bandiera = "<img src='/wp-content/uploads/custom/loghi-compagnie/" . $vettore . "120X20.JPG' height=12 border='0' />";

			$arr_codici_volo[] = $codice_volo;
			$arr_bandiera[] = $bandiera;
		}
		mysqli_free_result($rs2);

		#echo var_dump($arr_codici_volo);

		/*
		 * Creo il codice html dei codici volo
		 */
		$y = 0;
		if (count($arr_codici_volo) == 1) {
			$html_code_codici_volo = '<td width="55">' . $arr_codici_volo[0] . '</td>';
		} else {

			$html_code_codici_volo = '<td width="55">' . "\n";
			foreach ($arr_codici_volo as $codice_volo) {
				$html_code_codici_volo .= '<span id="cv' . $i . '.' . $y . '">' . $codice_volo . '</span>' . "\n";
				$y++;
			}
			$html_code_codici_volo .= '</td>' . "\n";

			$js_code .= 'cicla("cv' . $i . '", ' . $y . ', 0);' . "\n";
		}

		/*
		 * Creo il codice html delle bandiere delle compagnie aeree
		 */
		$y = 0;
		if (count($arr_bandiera) == 1) {
			$html_code_bandiere = '<td width="90">' . $arr_bandiera[0] . '</td>';
		} else {

			$html_code_bandiere = '<td width="90">' . "\n";
			foreach ($arr_bandiera as $bandiera) {
				$html_code_bandiere .= '<span id="bn' . $i . '.' . $y . '">' . $bandiera . '</span>' . "\n";
				$y++;
			}
			$html_code_bandiere .= '</td>' . "\n";

			$js_code .= 'cicla("bn' . $i . '", ' . $y . ', 0);' . "\n";
		}

		$html_code .= '
		<tr class="testo" height="28" bgcolor="' . $bgcolor . '">
			<td width="20%" style="text-align:left; "><strong>' . $destinazione_visibile . ' ' . $abbrevizione_aeroporto . '</strong></td>
			<td width="12%">' . $previsto . '</td>
			<td width="12%">' . $effettivo . '</td>
			' . $html_code_codici_volo . '
			' . $html_code_bandiere . '
			<td width="20%">' . $stato_volo . '</td>
		</tr>
		';

		$i++;
	}
}


echo $html_code;
?>

</table>
</div>
</div>

<script type="text/javascript">
	function cicla(name, i, visualizzato) {

		if (visualizzato >= i) {
			y = 0;
		} else {
			y = visualizzato + 1;
		}

		var el = name + '.' + y
		var pollo = document.getElementById(el);

		if (pollo) {
			pollo.style.display = '';
			for (j = 0; j < i; j++) {
				if (j != y) {
					var ciccio = name + '.' + j;
					document.getElementById(ciccio).style.display = 'none';
				}
			}
		}
		setTimeout('cicla("' + name + '", ' + i + ', ' + y + ')', 2000);

	}
</script>

<?php
echo '
<script type="text/javascript">
' . $js_code . '
</script>
';
?>