<?php
echo '
<script type="text/javascript" src="/wp-content/uploads/custom/voliTempoReale/cicla.js"></script>
';
include("../configura_db.php");


$oggi = date('d-m-Y');
//$adesso = date('H:i');

$sql0 = "SELECT DATE_FORMAT(curtime(), '%H%i')";
$rs0 = mysqli_query($conn, $sql0);
$r = mysqli_fetch_row($rs0);
$adesso = $r[0];

echo '
<script type="text/javascript">
function ricarica(){
	location.reload();
}
</script>
';

// ICL_LANGUAGE_CODE è la variabile predefinita di WPML, restituisce il codice della lingua (es. "it", "en"...)
switch (ICL_LANGUAGE_CODE) {
	case 'en':
		//echo '<p>View the flight schedule in the: <a href="'.$url.'&fascia=mattino">MORNING</a> - <a href="'.$url.'&fascia=pomeriggio">AFTERNOON</a> - <a href="'.$url.'&fascia=sera">EVENING</a></p>';
		$Volo = 'Flight';
		$Provenienza = 'Origin';
		$Orario = 'Time';
		$Previsto = 'Expected';
		$Note = 'Note';
		$Compagnia = 'Airlines';
		break;
	default:
		//echo '<p>Visualizza l\'orario dei voli del: <a href="'.$url.'&fascia=mattino">MATTINO</a> - <a href="'.$url.'&fascia=pomeriggio">POMERIGGIO</a> - <a href="'.$url.'&fascia=sera">SERA</a></p>';
		$Volo = 'Volo';
		$Provenienza = 'Provenienza';
		$Orario = 'Orario';
		$Previsto = 'Previsto';
		$Note = 'Note';
		$Compagnia = 'Compagnia';
}

echo "
<div id='posizione'>
	<table width='100%' cellspacing=0 cellpadding=0 border='0' id='orario_arrivi' class='orariotemporeale'>
	<tr class='testo' style='background-color: #cd2122; text-transform: uppercase; color: #FFF; height: 40px;'>
		<th width='20%'>$Provenienza</th>
		<th width='12%'>$Orario</th>
		<th width='12%'>$Previsto</th>
		<th width='16%'>$Volo</th>
		<th width='20%'>$Compagnia</th>
		<th width='20%'>Status</th>
	</tr></table>
	";

echo "
<div id='inner-table-voli'><table width='100%' cellspacing=0 cellpadding=0 border='0' id='orario_arrivi_center' class='orariotemporeale'>

";

$oggi = date('Y-m-d');
//$adesso = date('Hi');
$fascia = $_GET['fascia'];

if (!isset($fascia) && strlen($fascia) <= 0) {
	$ora_attuale = date('H');
	$fascia = 'pomeriggio';
	if ($ora_attuale <= 13) $fascia = 'mattino';
	if ($ora_attuale >= 18) $fascia = 'sera';
}

# Modifica 10/12/2010
# i voli della notte del giorno prima vanno mostrati nella fascia oraria del mattino del giorno successivo
# modifico il WHERE in modo che:
# - la fascia del mattino prenda i voli odierni con schedulazione minore delle 13 e tutti i voli del giorno prima
# - la fascia del pomeriggio e della sera prendano solo i voli odierni

$ieri = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') - 1, date('Y')));

switch ($fascia) {
	case 'mattino':
		$where = " WHERE (schedulato < '1300' AND data='" . date('Y-m-d') . "') OR data='" . $ieri . "' ";
		break;
	case 'pomeriggio':
		$where = " WHERE schedulato >= '1300' AND schedulato <= '1800' AND data='" . date('Y-m-d') . "' ";
		break;
	case 'sera':
		$where = " WHERE schedulato > '1800' AND data='" . date('Y-m-d') . "' ";
		break;
}

#AS 20161109 - rimozione fasce
$where = " WHERE data='" . date('Y-m-d') . "' ";

$sql = "SELECT * FROM arrivi_new " . $where . " GROUP BY previsto, routing_uno,gate ORDER BY schedulato;";
//echo $sql;
$rs = mysqli_query($conn, $sql);
$sql2 = "SELECT count(*) FROM arrivi_new " . $where;
$rs2 = mysqli_query($conn, $sql2);
$num_righe = mysqli_fetch_row($rs2);
if ($num_righe[0] == 0) {
	switch (ICL_LANGUAGE_CODE) {
		case 'en':
			$html_code = "<tr><td>No flights scheduled</td></tr>";
			break;
		default:
			$html_code = "<tr><td>Non ci sono voli programmati</td></tr>";
	}
} else {
	while ($row_xy = mysqli_fetch_row($rs)) {
		//$destinazione = $row_xy[8];
		$destinazione = (ICL_LANGUAGE_CODE == 'en') ? $row_xy[9] : $row_xy[8];
		//$arr_destinazioni = explode(' ', $destinazione);
		//$stato_volo = $row_xy[12];
		$stato_volo = (ICL_LANGUAGE_CODE == 'en') ? $row_xy[17] : $row_xy[12];
		$previsto = substr($row_xy[10], 0, 2) . ':' . substr($row_xy[10], -2);
		$effettivo = substr($row_xy[11], 0, 2) . ':' . substr($row_xy[11], -2);
		$gate = $row_xy[20];

		$arr_codici_volo = array();
		$arr_bandiera = array();
		$arr_compagnia = array();
		$html_code_codici_volo = '';
		$html_code_bandiere = '';
		$html_code_compagnie = '';

		$sql = "SELECT volo_uno, vettore_uno, compagnia_uno FROM arrivi_new ";
		$sql .= "WHERE (routing_uno='" . $destinazione . "' OR routing_due='" . $destinazione . "') AND schedulato='" . $row_xy[10] . "'  AND gate='" . $gate . "'";
		$rs2 = mysqli_query($conn, $sql);
		while ($arr = mysqli_fetch_array($rs2)) {

			$bandiera = '';
			$vettore = $arr['vettore_uno'];
			$codice_volo = $arr['volo_uno'];
			$compagnia = $arr['compagnia_uno'];

			if ($vettore != '')
				$bandiera = "<img src='/wp-content/uploads/custom/loghi-compagnie/" . $vettore . "120X20.JPG' height=12 border='0' />";

			$arr_codici_volo[] = $codice_volo;
			$arr_bandiera[] = $bandiera;
			$arr_compagnia[] = $compagnia;
		}
		mysqli_free_result($rs2);

		/*
		 * Creo il codice html dei codici volo
		 */
		$y = 0;
		if (count($arr_codici_volo) == 1) {
			$html_code_codici_volo = '<td>' . $arr_codici_volo[0] . '</td>';
		} else {
			$html_code_codici_volo = '<td>' . "\n";
			foreach ($arr_codici_volo as $codice_volo) {
				$html_code_codici_volo .= '<span id="cv' . $i . '.' . $y . '">' . $codice_volo . '</span>' . "\n";
				$y++;
			}
			$html_code_codici_volo .= '</td>' . "\n";

			$js_code .= 'cicla("cv' . $i . '", ' . $y . ', 0);' . "\n";
		}

		/*
		 * Creo il codice html delle bandiere delle compagnie aeree
		 */
		$y = 0;
		if (count($arr_bandiera) == 1) {
			$html_code_bandiere = '<td width="20%">' . $arr_bandiera[0] . '</td>';
		} else {

			$html_code_bandiere = '<td width="20%">' . "\n";
			foreach ($arr_bandiera as $bandiera) {
				$html_code_bandiere .= '<span id="bn' . $i . '.' . $y . '">' . $bandiera . '</span>' . "\n";
				$y++;
			}
			$html_code_bandiere .= '</td>' . "\n";

			$js_code .= 'cicla("bn' . $i . '", ' . $y . ', 0);' . "\n";
		}

		/*
		 * Creo il codice html dei nomi delle compagnie aeree
		 */
		$y = 0;
		if (count($arr_compagnia) == 1) {
			$html_code_compagnie = '<td colspan="2">' . $arr_compagnia[0] . '</td>';
		} else {

			$html_code_compagnie = '<td colspan="2">' . "\n";
			foreach ($arr_compagnia as $compagnia) {
				$html_code_compagnie .= '<span id="ve' . $i . '.' . $y . '">' . $compagnia . '</span>' . "\n";
				$y++;
			}
			$html_code_compagnie .= '</td>' . "\n";

			$js_code .= 'cicla("ve' . $i . '", ' . $y . ', 0);' . "\n";
		}


		$html_code .= '
		<tr>
			<td width="20%"><strong>' . $destinazione . '</strong></td>
			<td width="12%">' . $previsto . '</td>
			<td width="12%">' . $effettivo . '</td>
			' . $html_code_codici_volo . '
			' . $html_code_bandiere . '
			<td width="20%">' . $stato_volo . '</td>
		</tr>
		';

		$i++;
	}
}
echo $html_code;

echo "</table></div>	
</div>";

switch (ICL_LANGUAGE_CODE) {
	case 'en':
		echo "<p align='right'>Last update at " . substr($adesso, 0, 2) . ":" . substr($adesso, 2) . " on " . $oggi . "</p>";
		break;
	default:
		echo "<p align='right'>Situazione aggiornata alle ore " . substr($adesso, 0, 2) . ":" . substr($adesso, 2) . " del " . $oggi . "</p>";
}

?>


<?= '<script type="text/javascript">' . $js_code . '</script>'; ?>
