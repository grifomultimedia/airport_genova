<?php
	include("../configura_db.php");

	if(isset($_GET["type"]))
	{
		$type=$_GET["type"];
	}
	else
	{
		$type="t"; // tutti
	}
	
	function parseToXML($htmlStr)
	{
		$xmlStr=str_replace('<','&lt;',$htmlStr);
		$xmlStr=str_replace('>','&gt;',$xmlStr);
		$xmlStr=str_replace('"','&quot;',$xmlStr);
		$xmlStr=str_replace("'",'&#39;',$xmlStr);
		$xmlStr=str_replace("&",'&amp;',$xmlStr);
		return $xmlStr;
	}
	
	$dom = new DOMDocument("1.0");
	$node = $dom->createElement("markers");
	$parnode = $dom->appendChild($node);

	
	$sql_geo="SELECT * FROM mappa_voli ";
	if($type!='t')
	{
		$sql_geo.=" WHERE type='".$type."' ";
	}
	
	$result_geo=mysqli_query($conn, $sql_geo) or die("PROBLEMA CONNESSIONE");
   
   header("Content-type: text/xml"); 
  
   while($row_geo = @mysqli_fetch_assoc($result_geo))
   {
	
		$paese=$row_geo["name"];
		$id_volo=$row_geo["id"];
		$sql_compagnie="SELECT logo_compagnia,sito_internet FROM compagnie_aeree ca,compagnie_aeree_has_mappa_voli camv WHERE camv.id_compagnie_aeree=ca.id_compagnie_aeree AND camv.id_volo=$id_volo ";
		$rs_compagnie=mysqli_query($conn, $sql_compagnie);
		$dati="";
		while($row_dati=mysqli_fetch_assoc($rs_compagnie))
		{
			$dati.="<a href='".$row_dati['sito_internet']."' target='_blank' color='#ffffff' ><img src='".$row_dati['logo_compagnia']."' height='40px;' style='border: 0;' /></a><br/>";
		}
		
		/*$lat=$row_geo["lat"];
		$lng=$row_geo["lng"];
		$type_geo=$row_geo["type"];
		*/
		
		$node = $dom->createElement("marker");
  	$newnode = $parnode->appendChild($node);
  	$newnode->setAttribute("name",$paese);
  	$newnode->setAttribute("address", $dati);
  	$newnode->setAttribute("lat", $row_geo['lat']);
  	$newnode->setAttribute("lng", $row_geo['lng']);
  	$newnode->setAttribute("type", $row_geo['type']);
		
	}	
	
	echo $dom->saveXML();
?>
