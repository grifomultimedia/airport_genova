<?php
include("../configura_db.php");

if(isset($_GET["type"]))
{
	$type=$_GET["type"];
}
else
{
	$type="t"; // tutti
}

function parseToXML($htmlStr) 
{ 
$xmlStr=str_replace('<','&lt;',$htmlStr); 
$xmlStr=str_replace('>','&gt;',$xmlStr); 
$xmlStr=str_replace('"','&quot;',$xmlStr); 
$xmlStr=str_replace("'",'&#39;',$xmlStr); 
$xmlStr=str_replace("&",'&amp;',$xmlStr); 
return $xmlStr; 
} 

?>

		<?php 
	
		$sql_geo="SELECT * FROM mappa_voli ";
		if($type!='t')
		{
			$sql_geo.=" WHERE type='".$type."' ";
		}
		
		$result_geo=mysql_query($sql_geo,$conn) or die("PROBLEMA CONNESSIONE");
	   
	   header("Content-type: text/xml"); 
	  
	   echo "<markers>";
	   
	   while($row_geo = @mysql_fetch_assoc($result_geo))
	   {
		
			$paese=$row_geo["name"];
			$id_volo=$row_geo["id"];
			$sql_compagnie="SELECT logo_compagnia,sito_internet FROM compagnie_aeree ca,compagnie_aeree_has_mappa_voli camv WHERE camv.id_compagnie_aeree=ca.id_compagnie_aeree AND camv.id_volo=$id_volo ";
			$rs_compagnie=mysql_query($sql_compagnie,$conn);
			$dati="";
			while($row_dati=mysql_fetch_assoc($rs_compagnie))
			{
				$dati.="<a href='".$row_dati['sito_internet']."' target='_blank' color='#ffffff' ><img src='http://www.airport.genova.it".$row_dati['logo_compagnia']."' height='40px;'  style='border: 0;' /></a><br/>";
			}
			
			
			
			$lat=$row_geo["lat"];
			$lng=$row_geo["lng"];
			$type_geo=$row_geo["type"];
			
			echo '<marker ';
			echo 'name="' . parseToXML($paese) . '" ';
			echo 'address="' . parseToXML($dati).'" ';
			echo 'lat="' . $lat . '" ';
			echo 'lng="' . $lng . '" ';
			echo 'type="' . $type_geo. '" ';
			echo '/>';
		}	
			echo '</markers>';	
		
	?>
