<?php
include('./httpful.phar');


echo "<link rel='stylesheet' href='../../../themes/kallyas-child/style.css' type='text/css'>";

$searchCode=$_POST['searchCode'];
$language=$_POST['language'];
//echo $language;
//LABELS
$labels;
$errorMsg;
$labelsIT=["Numero internet e call center","Bagaglio numero","Etichetta","Cognome","Compagnia","Codice ricerca","Stato del bagaglio","Tipologia di ritiro"];
$labelsEN=["Web and call center code","Baggage number","Baggage Tag","Last Name","Airline","Serach Code","Baggage Status","Delivery Type"];

//if ( isset($_GET['lang']) ) 
	//ICL_LANGUAGE_CODE = $_GET['lang'];
//if ( !isset(ICL_LANGUAGE_CODE) ) 
	//ICL_LANGUAGE_CODE = 'it';

//URI DI TEST
//$uri="http://alfarestws.airport.genova.it/api/AHLStatus?SearchCode=967104";
//$uri="http://alfarestws.airport.genova.it/api/AHLStatus?SearchCode=967113";
//$uri="http://alfarestws.airport.genova.it/api/AHLStatus?SearchCode=967068";
//$uri="http://alfarestws.airport.genova.it/api/AHLStatus?SearchCode=99999";
//$searchCode=967113;
//FINE URI DI TEST

$uri="http://alfarestws.airport.genova.it/api/AHLStatus?SearchCode=".$searchCode;
$response = \Httpful\Request::get($uri)->send();
$data = json_decode($response);
switch($language)
{
	case 'en':
                $labels=$labelsEN;
               	$errorMsg=$data->ErrorMessageEN;
                break;
        default:
		$labels=$labelsIT;
                $errorMsg=$data->ErrorMessageIT;
}


if( $data->StatusCode != 0 )
{
	switch($language)
        {
                case 'en':
                        echo $data->ErrorMessageEN;
                break;
                default:
                        echo $data->ErrorMessageIT;
        }
}
else
{
	$baggages=$data->Baggages;
	echo "<table class='lostefound_table' width='100%' cellspacing=0 cellpadding=0 border='0'>";
	foreach ($baggages as $baggage)
	{
    		
		//WTFileCode
		echo "<tr style='background-color: #cd2122; text-transform: uppercase; color: #FFF; height: 40px; font-weight:bold;'>";
		echo "<td width='30%' style='padding:5px; border: 2px solid white;'>".$labels[0]."</td>";
		//echo "<td style='padding:5px; border: 2px solid white;'>".$baggage->WTFileCode."</td>";
                echo "<td style='padding:5px; border: 2px solid white;'>".$baggage->SearchCode."</td>";
		echo "</tr>";

		//BagProgressive
		echo "<tr style='background-color: #f5f5f5;  color: #535353;'>";
		echo "<td style='padding:5px; border: 2px solid white;'>".$labels[1]."</td>";
		echo "<td style='padding:5px; border: 2px solid white;'>".$baggage->BagProgressive."</td>";
		echo "</tr>";

		//BagTag
		echo "<tr style='background-color: #f5f5f5;  color: #535353;'>";
		echo "<td style='padding:5px; border: 2px solid white;'>".$labels[2]."</td>";
		echo "<td style='padding:5px; border: 2px solid white;'>".$baggage->BagTag."</td>";
		echo "</tr>";

		//PaxName
		echo "<tr style='background-color: #f5f5f5; color: #535353;'>";
		echo "<td style='padding:5px; border: 2px solid white;'>".$labels[3]."</td>";
		echo "<td style='padding:5px; border: 2px solid white;'>".$baggage->PaxName."</td>";
		echo "</tr>";

		//Airline
		echo "<tr style='background-color: #f5f5f5; color: #535353; '>";
		echo "<td style='padding:5px; border: 2px solid white;'>".$labels[4]."</td>";
		echo "<td style='padding:5px; border: 2px solid white;'>".$baggage->Airline."</td>";
		echo "</tr>";

		//SearchCode
		//echo "<tr style='background-color: #f5f5f5; color: #535353;'>";
		//echo "<td style='padding:5px; border: 2px solid white;'>".$labels[5]."</td>";
		//echo "<td style='padding:5px; border: 2px solid white;'>".$baggage->SearchCode."</td>";
		//echo "</tr>";

		//BagStatus
		echo "<tr style='background-color: #f5f5f5; color: #535353;'>";
		echo "<td style='padding:5px; border: 2px solid white;'>".$labels[6]."</td>";
		switch($language)
             	{
                	case 'en':
                        	echo "<td style='padding:5px; border: 2px solid white;'>".$baggage->BagStatusEN."</td>";
                    		break;
                     	default:
                             	echo "<td style='padding:5px; border: 2px solid white;'>".$baggage->BagStatusIT."</td>";
             	}
		echo "</tr>";

		//Delivery
		echo "<tr style='background-color: #f5f5f5; color: #535353;'>";
		echo "<td style='padding:5px; border: 2px solid white;'>".$labels[7]."</td>";
             	switch($language)
		{
                	case 'en':
                        	echo "<td style='padding:5px; border: 2px solid white;'>".$baggage->DeliveryTypeEN."</td>";
                    		break;
                     	default:
                             	echo "<td style='padding:5px; border: 2px solid white;'>".$baggage->DeliveryTypeIT."</td>";
             	}
		echo "</tr>";

	}
	echo "</table>";
}


?>
