<?php

define('DB_NAME', 'aerogenova');
define('DB_USER', 'root');
define('DB_PASSWORD', '');

define('DB_HOST', '127.0.0.1');
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');

/**
 *	{@link https://api.wordpress.org/secret-key/1.1/salt/ servizio di chiavi-segrete di WordPress.org}
 */
define('AUTH_KEY',         '');
define('SECURE_AUTH_KEY',  '');
define('LOGGED_IN_KEY',    '');
define('NONCE_KEY',        '');
define('AUTH_SALT',        '');
define('SECURE_AUTH_SALT', '');
define('LOGGED_IN_SALT',   '');
define('NONCE_SALT',       '');

$table_prefix  = 'ag_';

// define('FORCE_SSL_ADMIN', true);
//define('WP_DEBUG', true);
//define('WP_DEBUG_DISPLAY', true);
//define('WP_DEBUG_LOG', true);
//define('SCRIPT_DEBUG', true);
//define('CONCATENATE_SCRIPTS', false);

//define('WP_HOME', 'http://aerogenova.grifomultimedia.it');
//define('WP_SITEURL', 'http://aerogenova.grifomultimedia.it');

/* Finito, interrompere le modifiche! Buon blogging. */

/** Path assoluto alla directory di WordPress. */
if (!defined('ABSPATH'))
	define('ABSPATH', dirname(__FILE__) . '/');

/** Imposta le variabili di WordPress ed include i file. */
require_once(ABSPATH . 'wp-settings.php');
